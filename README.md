Sample script that push notification with advice to extend the battery life.

- "Battery is over 90%, it's time to unplug!"
- "Battery is less than 20%, it's time to plugin!"


# Install

Create the binary
```sh
pyinstaller --onefile batteryalert.py
```

Run the binary
```sh
sudo cp dist/batteryalert /usr/bin
batteryalert
```

Setup cron to run batteryalert every 15 minutes
```sh
$ crontab -e

*/15 * * * * XDG_RUNTIME_DIR=/run/user/$(id -u) batteryalert
```
