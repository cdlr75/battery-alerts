#!/usr/bin/env python
#-*- coding: utf-8 -*-

import subprocess
import psutil


def notify(message):
    subprocess.Popen("notify-send -u critical batteryalert".split(" ") + [message])


def main():
  battery = psutil.sensors_battery()

  if battery.percent > 90 and battery.power_plugged:
    notify("Battery is over 90%, it's time to unplug!")

  if battery.percent < 20 and not battery.power_plugged:
    notify("Battery is less than 20%, it's time to plugin!")


if __name__ == '__main__':
    main()
